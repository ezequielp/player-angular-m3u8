import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import m3u8Parser from 'node_modules/m3u8-parser';
import { Url } from 'url';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class VjsPlayerService {
  constructor(private http: HttpClient) { }

  clipUrl = 'https://jvntqbny1e.execute-api.us-east-1.amazonaws.com/dev/ffmpeg';


  clip(post: any): Observable<any> {
    return this.http.post<any>(this.clipUrl, post, httpOptions);
  }


  generateM3u8(source: string, timeFrom: number, timeTo: number): any {


    var manifest = this.http.get(source, { responseType: "text" });
    var url = source.substr(0, source.lastIndexOf('/'));

    return new Promise((resolve, reject) => {
      manifest.subscribe((data) => {
        this.splitIntoSegments(data, url, timeFrom, timeTo, resolve);
      });
    });

  }

  splitIntoSegments(data: string, url: string, timeFrom, timeTo, resolve): any {
    var parser = new m3u8Parser.Parser();

    /*
    parser.addParser({
      expression: /#VOD-TIMING/,
      customType: 'vodTiming',
      segment: true
    });
  */

    var totalDuration = 0;
    parser.push(data);
    parser.end();


    if (parser.manifest.playlists) {
      // Is a master playlist

      let maxBandwith = Math.max.apply(Math, parser.manifest.playlists.map(function (o) {
        return o.attributes.BANDWIDTH;
      }));

      var playlistMax;

      for (let playlist of parser.manifest.playlists) {
        if (playlist.attributes.BANDWIDTH == maxBandwith) {
          playlistMax = playlist;
        }
      }

      // Chequeamos si la ruta es absoluta o relativa
      var playlistUrl = "";
      var pat = /^https?:\/\/|^\/\//i;
      if (pat.test(playlistMax.uri)) {
        // Es absoluta
        playlistUrl = playlistMax.uri;
      }
      else {
        // Relativa usamos entonces la URL heredada de master
        playlistUrl = url + '/' + playlistMax.uri;
      }

      // Obtenemos el dato
      var manifest = this.http.get(playlistUrl, { responseType: "text" });

      manifest.subscribe((data) => {

        this.splitIntoSegments(data, playlistUrl, timeFrom, timeTo, resolve);
      });
    }
    else {
      const manifestArray = [
        '#EXTM3U',
        '#EXT-X-VERSION:3'
      ];

      var urlMaster = url;
      var url = url.substr(0, url.lastIndexOf('/'));

      manifestArray.push('#EXT-X-TARGETDURATION:' + parser.manifest.targetDuration);
      manifestArray.push('#EXT-X-MEDIA-SEQUENCE:' + parser.manifest.mediaSequence);

      var totalSegments = 0;
      for (let segment of parser.manifest.segments) {

        var segmentUrl = "";
        var pat = /^https?:\/\/|^\/\//i;
        if (pat.test(segment.uri)) {
          // Es absoluta
          segmentUrl = segment.uri;
        }
        else {
          // Relativa usamos entonces la URL heredada de master
          segmentUrl = url + '/' + segment.uri;
        }

        // Here filter by datetime (calculated)

        // voy incrementanto totalDuration += segment.duration

        // chequeo si totalDuration >= timeStart --> TRUE
        // &
        // chequeo si totalDuration <=  timeTo --> TRUE

        if (totalDuration < timeTo && totalDuration > timeFrom) {
          manifestArray.push('#EXTINF:' + segment.duration);
          manifestArray.push(segmentUrl);
        }

        totalDuration += segment.duration;
        totalSegments++;
      }
      manifestArray.push('#EXT-X-ENDLIST');

      resolve({ content: manifestArray.join('\n'), urlMaster: urlMaster });

      // is a regular playlist
    }


  }




}

