// vjs-player.component.ts
import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, SimpleChanges, ViewChild, ViewEncapsulation } from '@angular/core';
import videojs from 'video.js';
import { VjsPlayerService } from './vjs-player.service';


@Component({
  selector: 'app-vjs-player',
  templateUrl: './vjs-player.component.html',
  styleUrls: [
    './vjs-player.component.scss'
  ],
  encapsulation: ViewEncapsulation.None,
})
export class VjsPlayerComponent implements OnInit, OnDestroy {

  @Input() channel: any;
  @Input() time: any;
  @Output() clipSelectionEmmiter = new EventEmitter<any>();
  @ViewChild('target', { static: true }) target: ElementRef;

  source: string = "";
  urlMaster: string = "";
  player: videojs.Player;

  constructor(
    private elementRef: ElementRef,
    protected vjsService: VjsPlayerService
  ) { }

  public style: object = {};


  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges) {

    this.source = this.channel.m3u8;
    if (this.source) {
      this.loadUrl();
    }
  }


  loadUrl() {
    let options;
    options = {};
    options.loop = true;
    options.autoplay = true;
    options.muted = true;
    options.fluid = true;
    options.liveui = false;
    options.live = false;
    options.sources = [];
    options.sources[0] = { src: this.source, type: "application/x-mpegURL" };
    options.controls = false;
  }

  ngOnDestroy() {
    // destroy player
    if (this.player) {
      this.player.dispose();
    }
  }
}